package com.sumerge.test;

import static org.junit.Assert.assertTrue;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.sumerge.program.models.GroupBasicModel;
import com.sumerge.program.models.GroupFullModel;
import com.sumerge.program.models.UserExtendedModel;
import com.sumerge.program.models.UserFullModel;
import com.sumerge.program.rest.GroupClient;
import org.junit.BeforeClass;
import org.junit.Test;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import com.sumerge.program.rest.UserClient;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


public class UserResourcesIT {

    private static UserClient userClient;
    private static GroupClient groupClient;

    @BeforeClass
    public static void init(){
        List<Object> providers = new ArrayList<Object>();
        providers.add(new JacksonJaxbJsonProvider());
        userClient = JAXRSClientFactory.create("http://localhost:8880/myapp",
                UserClient.class, providers,"defaultAdmin","12",null);
        groupClient = JAXRSClientFactory.create("http://localhost:8880/myapp",
                GroupClient.class, providers,"defaultAdmin","12",null);
    }

    @Test
    public void getUserByUsernameTest(){
        Response response = userClient.getUserByUsername("defaultAdmin");
        assertTrue("Incorrect Response", response.getStatus() == 200);
        UserFullModel userFullModel = response.readEntity(UserFullModel.class);
        assertTrue("Wrong User Returned", userFullModel.getRole().equals("admin")
        && userFullModel.getName().equals("Default Admin"));
    }

    @Test
    public void addUserTest(){
        UserExtendedModel userExtendedModel = new UserExtendedModel();
        userExtendedModel.setUsername("XXX_123");
        userExtendedModel.setName("xxx");
        userExtendedModel.setRole("user");
        userExtendedModel.setEmail("xxx@gmail.com");
        userExtendedModel.setAddress("2, 2nd st");
        userExtendedModel.setPassword("6b51d431df5d7f141cbececcf79edf3dd861c3b4069f0b11661a3eefacbba918");

        Response response = userClient.addUser(userExtendedModel);
        assertTrue("Incorrect Response",response.getStatus()==200);

        Response response1 = userClient.getUserByUsername("XXX_123");
        UserFullModel userFullModel = response1.readEntity(UserFullModel.class);
        assertTrue("User Details are incorrect", userExtendedModel.equals(userFullModel));
    }

    @Test
    public void updateUserTest(){
        UserExtendedModel userExtendedModel = new UserExtendedModel();
        userExtendedModel.setAddress("3, 3rd st");
        userExtendedModel.setPhoneNo("0100110011");
        userExtendedModel.setName("xxx 123");
        userExtendedModel.setRole("admin");

        Response response = userClient.updateUser("XXX_123",userExtendedModel);
        assertTrue("Incorrect Response", response.getStatus()==200);

        Response response1 = userClient.getUserByUsername("XXX_123");
        UserFullModel userFullModel = response1.readEntity(UserFullModel.class);
        assertTrue("User updates are incorrect", userFullModel.getAddress().equals("3, 3rd st")
        && userFullModel.getPhoneNo().equals("0100110011")
        && userFullModel.getName().equals("xxx 123")
        && userFullModel.getRole().equals("admin")
        && userFullModel.getEmail().equals("xxx@gmail.com"));
    }

    @Test
    public void addGroupTest(){
        GroupBasicModel groupBasicModel = new GroupBasicModel();
        groupBasicModel.setName("Group_123");
        groupBasicModel.setDescription("This is a group for testing");
        Response response = groupClient.addGroup(groupBasicModel);
        assertTrue("Incorrect Response for Add Group",response.getStatus()==200);

        Response response1 = groupClient.getGroupByName("Group_123");
        assertTrue("Incorrect Response for Get Group", response1.getStatus()==200);
        GroupFullModel groupFullModel = response1.readEntity(GroupFullModel.class);
        assertTrue("Incorrect Details entered", groupFullModel.getName().equals("Group_123"));
    }

    @Test
    public void addUserToGroupTest(){
        Response response = userClient.addUserToGroup("XXX_123", "Group_123");
        assertTrue("Incorrect Response", response.getStatus()==200);

        Response response1 = userClient.getUserByUsername("XXX_123");
        UserFullModel userFullModel = response1.readEntity(UserFullModel.class);
        assertTrue("Failed To add Group to User", userFullModel.getGroups() != null
                && userFullModel.getGroups().size()==1);
    }

    @Test
    public void moveUserFromToGroupTest(){
        Response response = userClient.moveUserFromToGroup("XXX_123", "Group_123","Default Group");
        assertTrue("Incorrect Response", response.getStatus()==200);

        Response response1 = userClient.getUserByUsername("XXX_123");
        UserFullModel userFullModel = response1.readEntity(UserFullModel.class);
        assertTrue("Failed To move User between groups", userFullModel.getGroups() != null
                && userFullModel.getGroups().size()==1
                && userFullModel.getGroups().get(0).getName().equals("Default Group"));
    }

    @Test
    public void removeUserFromGroupTest(){
        Response response = userClient.removeUserFromGroup("XXX_123", "Default Group");
        assertTrue("Incorrect Response", response.getStatus()==200);

        Response response1 = userClient.getUserByUsername("XXX_123");
        UserFullModel userFullModel = response1.readEntity(UserFullModel.class);
        assertTrue("Failed To remove User from groups", userFullModel.getGroups().size()==0);
    }

    @Test
    public void deleteUserTest() {
        Response response = userClient.deleteUser("XXX_123");
        assertTrue("Incorrect Response", response.getStatus()==200);

        Response response1 = userClient.getUserByUsername("XXX_123");
        UserFullModel userFullModel = response1.readEntity(UserFullModel.class);
        assertTrue("User was not deleted", userFullModel.isDeleted());
    }

    @Test(expected = BadRequestException.class)
    public void updateMyProfileTest(){
        UserExtendedModel userExtendedModel = new UserExtendedModel();
        userExtendedModel.setAddress("3, 3rd st");
        userExtendedModel.setPhoneNo("0100110011");
        userClient.updateMyProfile(userExtendedModel);
    }
}

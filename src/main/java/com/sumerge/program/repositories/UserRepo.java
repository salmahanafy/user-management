package com.sumerge.program.repositories;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sumerge.program.entities.ActionType;
import com.sumerge.program.entities.Auditlog;
import com.sumerge.program.entities.Group;
import com.sumerge.program.entities.User;
import com.sumerge.program.interceptors.RepoInterceptor;
import com.sumerge.program.rest.exceptionhandlers.DataExceptions;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.List;

@Stateless
@Interceptors({RepoInterceptor.class})
public class UserRepo {
    @PersistenceContext(name = "UserManagement")
    EntityManager entityManager;

    @EJB
    GroupRepo groupRepo;

    public long getUserIdByUsername(String username){

        TypedQuery<Long> query = entityManager.createNamedQuery("getUserIdByUsername", Long.class);
        query.setParameter("username",username);
        return query.getSingleResult();
    }

    public List<User> getAllUsers(){
        TypedQuery<User> query = entityManager.createNamedQuery("getAllUsers",User.class);
        return query.getResultList();
    }

    public List<User> getAllNonDeletedUsers(){
        TypedQuery<User> query = entityManager.createNamedQuery("getAllNonDeletedUsers",User.class);
        return query.getResultList();
    }

    public User getUserByUsername(String username) throws DataExceptions{

        try {
            TypedQuery<User> query  = entityManager.createNamedQuery("getUserByUsername", User.class);
            query.setParameter("username", username);
            return query.getSingleResult();
        }catch (NoResultException e){
            throw new DataExceptions("No User Found with Username:  `"+ username+"`");
        }

    }

    public User getNonDeletedUserByUsername(String username) throws DataExceptions{

        try {
            TypedQuery<User> query = entityManager.createNamedQuery("getNonDeletedUserByUsername", User.class);
            query.setParameter("username", username);
            return query.getSingleResult();
        }catch (NoResultException e){
            throw new DataExceptions("No User Found with Username:  `"+ username+"`");
        }
    }

    private Auditlog createAuditLog(User user, String currentUsername,ActionType actionType){

        ObjectMapper mapper = new ObjectMapper();
        String entityState = null;
        try {
            entityState = mapper.writeValueAsString(user);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        long currentUser = getUserIdByUsername(currentUsername);
        return new Auditlog(entityState,currentUser,actionType,User.EntityType,user.getUserId());
    }

    @Transactional
    public void createUser(User user, String currentUsername)throws DataExceptions{

        try {
            if (user.getUsername()==null)
                throw new DataExceptions("Username can not be null");
            if (user.getName()==null)
                throw new DataExceptions("Name can not be null");
            if (user.getEmail()==null)
                throw new DataExceptions("Email can not be null");
            if (user.getPassword()==null)
                throw new DataExceptions("Password can not be null");
            if (user.getRole()==null)
                throw new DataExceptions("Role can not be null");

            entityManager.persist(user);
            entityManager.flush();

            Auditlog auditlog = createAuditLog(user, currentUsername, ActionType.add_user);
            entityManager.persist(auditlog);
        }catch (PersistenceException e){
            throw new DataExceptions("Username `"+user.getUsername()+"` already exists");
        }
    }

    @Transactional
    public void deleteUser(String username, String currentUsername)throws DataExceptions{

        if(username.equals(User.defaultAdminUsername))
            throw new DataExceptions("Deleting `"+User.defaultAdminUsername+"` is NOT Allowed");

        User user = getNonDeletedUserByUsername(username);
        user.setDeleted(true);
        user.setGroups(null);
        entityManager.merge(user);

        Auditlog auditlog = createAuditLog(user, currentUsername, ActionType.delete_user);
        entityManager.persist(auditlog);
    }

    @Transactional
    public void addUserToGroup(String username, String groupname, String currentUsername )throws DataExceptions{
        try {
            User user = getNonDeletedUserByUsername(username);
            Group group = groupRepo.getGroupByName(groupname);
            user.getGroups().add(group);
            entityManager.merge(user);

            Auditlog auditlog = createAuditLog(user, currentUsername, ActionType.add_user_to_group);
            entityManager.persist(auditlog);
        }catch (PersistenceException e){
            throw new DataExceptions("User: `"+username+"` is already a member of Group: `"+groupname+"`");
        }
    }

    @Transactional
    public void removeUserFromGroup(String username, String groupName, String currentUsername)throws DataExceptions{
        if(username.equals(User.defaultAdminUsername) && groupName.equals(User.defaultGroupName))
            throw new DataExceptions("Removing `"+User.defaultAdminUsername+"` from `"+User.defaultGroupName+"` is NOT Allowed");

        User user = getNonDeletedUserByUsername(username);
        Group group = groupRepo.getGroupByName(groupName);
        List<Group>groups= user.getGroups();
        int initSize = groups.size();
        groups.remove(group);
        if (groups.size()==initSize)
            throw new DataExceptions("User: `"+username+"` is NOT a member of Group: `"+groupName+"`");

        entityManager.merge(user);

        Auditlog auditlog = createAuditLog(user, currentUsername, ActionType.remove_user_from_group);
        entityManager.persist(auditlog);
    }

    @Transactional
    public void moveUserFromToGroup(String username,String fromGroupName,  String toGroupName, String currentUsername) throws DataExceptions{

        addUserToGroup(username, toGroupName, currentUsername);
        removeUserFromGroup( username,  fromGroupName,  currentUsername);
    }

    @Transactional
    public void updateUserDetails(String username, User user, String currentUsername)throws DataExceptions{
        if(username.equals(User.defaultAdminUsername))
            throw new DataExceptions("Updating `"+User.defaultAdminUsername+"` is NOT Allowed");

        try {
            User orgUser = getNonDeletedUserByUsername(username);
            if (user.getUsername() != null)
                orgUser.setUsername(user.getUsername());
            if (user.getName() != null)
                orgUser.setName(user.getName());
            if (user.getEmail() != null)
                orgUser.setEmail(user.getEmail());
            if (user.getAge() != null)
                orgUser.setAge(user.getAge());
            if(user.getPhoneNo() != null)
                orgUser.setPhoneNo(user.getPhoneNo());
            if (user.getAddress()!=null)
                orgUser.setAddress(user.getAddress());
            if(user.getRole() != null && !username.equals(currentUsername))
                orgUser.setRole(user.getRole());
            entityManager.merge(orgUser);

            Auditlog auditlog = createAuditLog(orgUser, currentUsername, ActionType.update_user);
            entityManager.persist(auditlog);

        }catch (PersistenceException e) {
            throw new DataExceptions("Username `"+username+"` already exists");
        }
    }

    @Transactional
    public boolean resetPassword(String oldPassword, String newPassword, String currentUsername) throws DataExceptions {
        User user = getUserByUsername(currentUsername);
        if (oldPassword.equals(user.getPassword())){
            user.setPassword(newPassword);
            entityManager.merge(user);

            Auditlog auditlog = createAuditLog(user, currentUsername, ActionType.change_password);
            entityManager.persist(auditlog);
            return true;
        }
        return false;
    }
}

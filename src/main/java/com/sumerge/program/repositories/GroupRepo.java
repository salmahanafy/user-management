package com.sumerge.program.repositories;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sumerge.program.entities.ActionType;
import com.sumerge.program.entities.Auditlog;
import com.sumerge.program.entities.Group;
import com.sumerge.program.interceptors.RepoInterceptor;
import com.sumerge.program.rest.exceptionhandlers.DataExceptions;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.List;

@Stateless
@Interceptors({RepoInterceptor.class})
public class GroupRepo {

    @PersistenceContext(name = "UserManagement")
    EntityManager entityManager;

    @EJB
    UserRepo userRepo;

    public Group getGroupByName(String name)throws DataExceptions{
        try{
            TypedQuery<Group> query = entityManager.createNamedQuery("getGroupByName",Group.class);
            return query.setParameter("name", name).getSingleResult();
        }catch (NoResultException e){
            throw new DataExceptions("No Group Found with Name: `"+name+"`", 404);
        }

    }

    public List<Group> getAllGroups(){
        TypedQuery<Group> query = entityManager.createNamedQuery("getAllGroups",Group.class);
        return query.getResultList();
    }

    private Auditlog createAuditLog(Group group, long currentUser,ActionType actionType){
        ObjectMapper mapper = new ObjectMapper();
        String entityState = null;
        try {
            entityState = mapper.writeValueAsString(group);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return new Auditlog(entityState,currentUser,actionType,Group.EntityType,group.getGroupId());
    }

    @Transactional
    public void createGroup(Group group, String currentUsername)throws DataExceptions {
        try {
            long creatorId = userRepo.getUserIdByUsername(currentUsername);
            group.setCreatorId(creatorId);
            entityManager.persist(group);
            entityManager.flush();

            Auditlog auditlog = createAuditLog(group, creatorId, ActionType.create_group);
            entityManager.persist(auditlog);
        }catch (PersistenceException e){
            throw new DataExceptions("Group with Name: `"+group.getName()+"` already exists");
        }
    }
}

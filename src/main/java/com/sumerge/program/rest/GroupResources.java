package com.sumerge.program.rest;

import com.sumerge.program.entities.Group;
import com.sumerge.program.models.GroupBasicModel;
import com.sumerge.program.models.GroupFullModel;
import com.sumerge.program.rest.exceptionhandlers.DataExceptions;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import com.sumerge.program.repositories.GroupRepo;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.List;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

@RequestScoped
public class GroupResources implements GroupClient {

    private static final Logger LOGGER = Logger.getLogger(GroupResources.class.getName());
    private static ModelMapper modelMapper = new ModelMapper();
    @EJB
    GroupRepo groupRepo;

    @Context
    SecurityContext securityContext;

    @Override
    public Response getAllGroups(){

        List<Group> groups = groupRepo.getAllGroups();
        java.lang.reflect.Type targetListType = new TypeToken<List<GroupBasicModel>>(){}.getType();
        List<GroupBasicModel> groupBasicModels = modelMapper.map(groups,targetListType);
        return Response.ok().entity(groupBasicModels).build();
    }

    @Override
    public Response addGroup(GroupBasicModel groupBasicModel) throws DataExceptions {

        String username = securityContext.getUserPrincipal().toString();
        Group group = modelMapper.map(groupBasicModel,Group.class);
        groupRepo.createGroup(group, username);
        return Response.ok().build();
    }

    @Override
    public Response getGroupByName(String name)throws DataExceptions{

            GroupBasicModel groupBasicModel;
            boolean admin = securityContext.isUserInRole("admin");
            Group group = groupRepo.getGroupByName(name);
            if (admin)
                groupBasicModel = modelMapper.map(group, GroupFullModel.class);
            else
                groupBasicModel = modelMapper.map(group, GroupBasicModel.class);
            return Response.ok().entity(groupBasicModel).build();
    }
}

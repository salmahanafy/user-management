package com.sumerge.program.rest.config;

import com.sumerge.program.rest.GroupResources;
import com.sumerge.program.rest.UserResources;
import com.sumerge.program.rest.exceptionhandlers.DataExceptionHandler;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("myapp")
public class MyApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<>();
        classes.add(UserResources.class);
        classes.add(GroupResources.class);
        classes.add(DataExceptionHandler.class);
        return classes;
    }
}

package com.sumerge.program.rest.exceptionhandlers;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DataExceptionHandler implements ExceptionMapper<Exception>{

    @Override
    public Response toResponse(Exception e) {

        if (e.getCause().getClass() == DataExceptions.class) {
            DataExceptions dataExceptions = (DataExceptions) e.getCause();
            return Response.status(dataExceptions.getErrorCode())
                    .entity(e.getCause().getMessage()).build();
        }
        return Response.serverError()
                .entity(e.getMessage()).build();
    }
}

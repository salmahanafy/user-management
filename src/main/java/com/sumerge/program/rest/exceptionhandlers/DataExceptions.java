package com.sumerge.program.rest.exceptionhandlers;

import java.io.Serializable;

public class DataExceptions extends RuntimeException implements Serializable {

    private static final long serialVersionUID = 1L;
    private int  errorCode;

    public DataExceptions() {
    }

    public DataExceptions(String message) {
        super(message);
        this.errorCode = 400;
    }

    public DataExceptions(String message, Exception e) {
        super(message, e);
    }

    public DataExceptions(String message, int errorCode){
        super(message);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}

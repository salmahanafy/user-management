package com.sumerge.program.rest;

import com.sumerge.program.models.UserExtendedModel;
import com.sumerge.program.models.UserBasicModel;
import com.sumerge.program.models.UserFullModel;
import com.sumerge.program.rest.exceptionhandlers.DataExceptions;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import com.sumerge.program.repositories.UserRepo;
import com.sumerge.program.entities.User;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import java.util.List;

@RequestScoped
public class UserResources implements UserClient{
    private static ModelMapper modelMapper = new ModelMapper();
    @EJB
    UserRepo userRepo;

    @Context
    SecurityContext securityContext;

    @Override
    public Response getAllUsers(){

        List<UserBasicModel> userBasicModels;
        java.lang.reflect.Type targetListType;
        List<User> users;
        if (securityContext.isUserInRole("admin")){
            users = userRepo.getAllUsers();
            targetListType = new TypeToken<List<UserFullModel>>(){}.getType();
        }
        else{
            users = userRepo.getAllNonDeletedUsers();
            targetListType = new TypeToken<List<UserBasicModel>>(){}.getType();
        }
        userBasicModels = modelMapper.map(users, targetListType);
        return Response.ok().
                entity(userBasicModels).
                build();
    }

    @Override
    public Response addUser(UserExtendedModel userExtendedModel) throws DataExceptions{

        String currentUsername = securityContext.getUserPrincipal().toString();
        User user = modelMapper.map(userExtendedModel, User.class);
        userRepo.createUser(user, currentUsername);
        return Response.ok().build();


    }

    @Override
    public Response updateMyProfile(UserExtendedModel userExtendedModel)throws DataExceptions{

        String username = securityContext.getUserPrincipal().toString();
        User user = modelMapper.map(userExtendedModel, User.class);
        userRepo.updateUserDetails(username, user,username);
        return Response.ok().build();
    }

    @Override
    public Response getUserByUsername(String username)throws DataExceptions{

        boolean admin = securityContext.isUserInRole("admin");
        String currentUsername = securityContext.getUserPrincipal().toString();
        User user;
        UserBasicModel userBasicModel;
        if(admin || username.equals(currentUsername)) {
            user = userRepo.getUserByUsername(username);
            userBasicModel = modelMapper.map(user, UserFullModel.class);
        }
        else{
            user = userRepo.getNonDeletedUserByUsername(username);
            userBasicModel = modelMapper.map(user, UserBasicModel.class);
        }
        return Response.ok().entity(userBasicModel).build();
    }

    @Override
    public Response deleteUser(String username)throws DataExceptions{

        String currentUsername = securityContext.getUserPrincipal().toString();
        userRepo.deleteUser(username,currentUsername);
        return Response.ok().
                build();
    }

    @Override
    public Response updateUser(String username, UserExtendedModel userExtendedModel) throws DataExceptions{

        User user = modelMapper.map(userExtendedModel, User.class);
        String currentUsername = securityContext.getUserPrincipal().toString();
        userRepo.updateUserDetails(username, user, currentUsername);
        return Response.ok().build();
    }

    @Override
    public Response addUserToGroup(String username, String groupName)throws DataExceptions{

        String currentUsername = securityContext.getUserPrincipal().toString();
        userRepo.addUserToGroup(username, groupName, currentUsername);
        return Response.ok().build();
    }

    @Override
    public Response removeUserFromGroup(String username, String groupName)throws DataExceptions{

        String currentUsername = securityContext.getUserPrincipal().toString();
        userRepo.removeUserFromGroup(username,groupName,currentUsername);
        return Response.ok().build();
    }

    @Override
    public Response moveUserFromToGroup(String username,String fromGroupName,  String toGroupName)throws DataExceptions{
        String currentUsername = securityContext.getUserPrincipal().toString();
        userRepo.moveUserFromToGroup(username, fromGroupName, toGroupName,currentUsername);
        return Response.ok().build();
    }

    @Override
    public Response resetPassword(String oldPassword,String newPassword)throws DataExceptions{

        String currentUsername = securityContext.getUserPrincipal().toString();
        boolean succeeded = userRepo.resetPassword(oldPassword,newPassword,currentUsername);
        if (succeeded)
            return Response.ok().build();
        else
            return Response.status(Response.Status.BAD_REQUEST).entity("Incorrect Password").build();
    }
}

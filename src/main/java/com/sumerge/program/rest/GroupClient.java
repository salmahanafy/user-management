package com.sumerge.program.rest;

import com.sumerge.program.models.GroupBasicModel;
import com.sumerge.program.rest.exceptionhandlers.DataExceptions;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("groups")
public interface GroupClient {

    @GET
    Response getAllGroups();

    @POST
    Response addGroup(GroupBasicModel groupBasicModel)throws DataExceptions;

    @GET
    @Path("{name}")
    Response getGroupByName(@PathParam("name") String name)throws DataExceptions;

}

package com.sumerge.program.rest;

import com.sumerge.program.models.UserExtendedModel;
import com.sumerge.program.rest.exceptionhandlers.DataExceptions;


import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("users")
public interface UserClient {

    @GET
    Response getAllUsers();

    @POST
    Response addUser(UserExtendedModel userExtendedModel)throws DataExceptions;

    @PUT
    Response updateMyProfile(UserExtendedModel userExtendedModel)throws DataExceptions;

    @GET
    @Path("{username}")
    Response getUserByUsername(@PathParam("username") String username)throws DataExceptions;

    @DELETE
    @Path("{username}")
    Response deleteUser(@PathParam("username") String username)throws DataExceptions;

    @PUT
    @Path("{username}")
    Response updateUser(@PathParam("username") String username, UserExtendedModel userExtendedModel)throws DataExceptions;

    @POST
    @Path("user-groups/{username}/{groupname}")
    Response addUserToGroup(@PathParam("username") String username, @PathParam("groupname") String groupName)throws DataExceptions;

    @DELETE
    @Path("user-groups/{username}/{groupname}")
    Response removeUserFromGroup(@PathParam("username") String username, @PathParam("groupname") String groupName)throws DataExceptions;

    @PUT
    @Path("user-groups/{username}/{fromgroupname}/{togroupname}")
    Response moveUserFromToGroup(@PathParam("username") String username, @PathParam("fromgroupname") String fromGroupName
            , @PathParam("togroupname") String toGroupName)throws DataExceptions;

    @PUT
    @Path("password")
    Response resetPassword(@HeaderParam("oldPassword") String oldPassword, @HeaderParam("newPassword") String newPassword)throws DataExceptions;
}

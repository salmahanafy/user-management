package com.sumerge.program.interceptors;

import org.apache.log4j.Logger;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

public class RepoInterceptor {
    private Logger logger ;
    @AroundInvoke
    public Object intercept(InvocationContext context) throws Exception{
        logger = logger.getLogger(context.getMethod().getDeclaringClass());
        logger.debug("Entering Method: "+ context.getMethod().getName());
        Object result = context.proceed();
        logger.debug("Exiting Method: "+ context.getMethod().getName());
        return result;
    }
}

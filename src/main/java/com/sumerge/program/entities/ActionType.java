package com.sumerge.program.entities;

public enum ActionType {
    add_user,
    update_user,
    delete_user,
    create_group,
    update_group,
    add_user_to_group,
    remove_user_from_group,
    change_password
}

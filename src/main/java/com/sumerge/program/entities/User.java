package com.sumerge.program.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "getUserIdByUsername", query = "SELECT u.userId FROM User u WHERE u.username = :username"),
        @NamedQuery(name = "getUserByUsername", query = "SELECT u FROM User u WHERE u.username = :username"),
        @NamedQuery(name = "getAllUsers", query = "SELECT u FROM User u"),
        @NamedQuery(name = "getAllNonDeletedUsers", query = "SELECT u FROM User u WHERE u.deleted = false"),
        @NamedQuery(name = "getNonDeletedUserByUsername", query = "SELECT u FROM User u WHERE  u.username = :username AND u.deleted = false ")
})
public class User implements Serializable {

    public static final String defaultAdminUsername = "defaultAdmin";
    public static final String defaultGroupName = "Default Group";
    public static final String EntityType = "user";

    @Id
    @Column(name = "USERID",  updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long userId;

    @Column(name = "USERNAME", nullable = false, length = 45)
    private String username;

    @Column(name = "NAME", nullable = false, length = 45)
    private String name;

    @Column(name = "EMAIL", nullable = false, length = 45)
    private String email;

    @Column(name = "AGE")
    private Integer age;

    @Column(name = "PASSWORD", nullable = false, length = 45)
    private String password;

    @Column(name = "ROLE", nullable = false)
    private String role;

    @Column(name = "DELETED")
    private boolean deleted;

    @Column(name = "PHONENO")
    private String phoneNo;

    @Column(name = "ADDRESS")
    private String address;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "user_group",
            joinColumns = @JoinColumn(name = "USERID"),
            inverseJoinColumns = @JoinColumn(name = "GROUPID"))
    List<Group> groups;


    public User() {}

    public long getUserId() {
        return userId;
    }
    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    public List<Group> getGroups() {
        return groups;
    }
    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getAge() {
        return age;
    }
    public void setAge(Integer age) {
        this.age = age;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }

    public boolean getDeleted() {
        return deleted;
    }
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getPhoneNo() {
        return phoneNo;
    }
    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }
}

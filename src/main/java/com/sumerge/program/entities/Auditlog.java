package com.sumerge.program.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;


@Entity
public class Auditlog implements Serializable {

    //private static
    @Id
    @Column(name = "AUDITLOGID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int auditlogId;

    @Column(name = "ACTIONTYPE", nullable = false)
    private String actionType;

    @Column(name = "DATE")
    private Timestamp date;

    @Column(name = "AUTHOR")
    private long author;

    @Column(name = "ENTITYSTATE", nullable = false)
    private String entityState;

    @Column(name = "ENTITYTYPE")
    private String entityType;

    @Column(name = "ENTITYID")
    private long entityId;

    public Auditlog(){}

    public Auditlog (String entityState, long currentUser, ActionType actionType, String entityType, long entityID){
        this.actionType = actionType.toString();
        this.entityState = entityState;
        this.author = currentUser;
        this.date = new Timestamp(System.currentTimeMillis());
        this.entityType = entityType;
        this.entityId = entityID;
    }

    public int getAuditlogId() {
        return auditlogId;
    }

    public void setAuditlogId(int auditlogId) {
        this.auditlogId = auditlogId;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public long getAuthor() {
        return author;
    }

    public void setAuthor(long author) {
        this.author = author;
    }

    public String getEntityState() {
        return entityState;
    }

    public void setEntityState(String entityState) {
        this.entityState = entityState;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public long getEntityId() {
        return entityId;
    }

    public void setEntityId(long entityId) {
        this.entityId = entityId;
    }
}


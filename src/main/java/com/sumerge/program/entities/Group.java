package com.sumerge.program.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Table(name = "appgroup")
@Entity
@NamedQueries({
        @NamedQuery(name = "getGroupByName", query = "SELECT g FROM Group g WHERE g.name = :name"),
        @NamedQuery(name = "getAllGroups", query = "SELECT g FROM Group g")
})

public class Group implements Serializable {
    public static final String EntityType = "appgroup";

    @Id
    @Column(name = "GROUPID", updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long groupId;

    @Column(name = "NAME", nullable = false, length = 45)
    private String name;

    @Column(name = "DESCRIPTION", length = 45)
    private String description;

    //@ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class)
    //@JoinColumn(name = "CREATOR")
    @Column (name = "CREATOR")
    private long creatorId;

    @JsonIgnore
    @ManyToMany(mappedBy = "groups", fetch = FetchType.LAZY)
    private List<User> users;

    public Group(){}

    public long getGroupId() {
        return groupId;
    }
    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public long getCreatorId() {
        return creatorId;
    }
    public void setCreatorId(long creatorId) {
        this.creatorId = creatorId;
    }

    public List<User> getUsers() {
        return users;
    }
    public void setUsers(List<User> users) {
        this.users = users;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return groupId == group.groupId;
    }
}

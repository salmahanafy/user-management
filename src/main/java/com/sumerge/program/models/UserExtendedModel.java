package com.sumerge.program.models;

import java.util.Objects;

public class UserExtendedModel extends UserBasicModel {


    private String phoneNo;
    private String address;
    private String role;
    private String password;
    private boolean deleted;

    public UserExtendedModel() {
    }

    public void setPassword(String password){
        this.password = password;
    }

    public String getPassword(){
        return password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        //if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        UserExtendedModel that = (UserExtendedModel) o;
        return deleted == that.deleted &&
                Objects.equals(phoneNo, that.phoneNo) &&
                Objects.equals(address, that.address) &&
                role.equals(that.role) &&
                password.equals(that.password);
    }

}

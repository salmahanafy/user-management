package com.sumerge.program.models;

import java.util.List;

public class GroupFullModel extends GroupBasicModel {
    List<UserBasicModel> users;

    public GroupFullModel() {
    }

    public List<UserBasicModel> getUsers() {
        return users;
    }

    public void setUsers(List<UserBasicModel> users) {
        this.users = users;
    }
}

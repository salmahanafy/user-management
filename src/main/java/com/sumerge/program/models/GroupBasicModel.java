package com.sumerge.program.models;


import java.util.Objects;

public class GroupBasicModel {
    private String name;
    private String description;
    private long creatorId;



    public GroupBasicModel(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(long creatorId) {
        this.creatorId = creatorId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        //if (o == null || getClass() != o.getClass()) return false;
        GroupBasicModel that = (GroupBasicModel) o;
        return creatorId == that.creatorId &&
                name.equals(that.name) &&
                Objects.equals(description, that.description);
    }
}

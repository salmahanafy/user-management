package com.sumerge.program.models;

import java.util.Objects;

public class UserBasicModel {

    private String username;
    private String name;
    private String email;
    private Integer age;

    public UserBasicModel() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        //if (o == null || getClass() != o.getClass()) return false;
        UserBasicModel that = (UserBasicModel) o;
        return username.equals(that.username) &&
                name.equals(that.name) &&
                email.equals(that.email) &&
                Objects.equals(age, that.age);
    }
}

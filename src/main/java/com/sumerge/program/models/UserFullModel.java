package com.sumerge.program.models;

import java.util.List;

public class UserFullModel extends UserExtendedModel {
    private List<GroupBasicModel> groups;

    public UserFullModel() {
    }

    public List<GroupBasicModel> getGroups() {
        return groups;
    }

    public void setGroups(List<GroupBasicModel> groups) {
        this.groups = groups;
    }

}
